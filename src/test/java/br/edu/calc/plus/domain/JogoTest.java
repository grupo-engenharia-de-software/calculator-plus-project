package br.edu.calc.plus.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.DisplayName;

public class JogoTest {

    public JogoTest() {
    }

    @Test
    @DisplayName("Verificando se o método estaCerto retorna corretamente quando o resultado é igual a resposta")
    public void verificarSeOMetodoEstaCertoRetornaTrueQuandoResultadoIgualResposta() {
        //Cenário
        Jogo primeiroJogo = new Jogo();
        int resp = 10;
        int result = 10;

        //Executar
        primeiroJogo.setResposta(resp);
        primeiroJogo.setResultado(result);
        boolean estaCorreto = primeiroJogo.isCorrect();

        //Verificar
        Assertions.assertTrue(estaCorreto);
    }

    @Test
    @DisplayName("Verificando se o método getIdjogo retorna corretamente o Id")
    public void verificarSeOMetodogetIdJogoRetornaCorretamenteOId() {
        //Cenário
        Jogo segundoJogo = new Jogo();
        int id = 1;

        //Executar
        segundoJogo.setIdjogo(id);
        int idDoJogo = segundoJogo.getIdjogo();

        //Verificar
        Assertions.assertEquals(1, idDoJogo);
    }
}


