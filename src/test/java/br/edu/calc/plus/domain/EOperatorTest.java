/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package br.edu.calc.plus.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.DisplayName;

/**
 *
 * @author gabri
 */
public class EOperatorTest {
    
    public EOperatorTest() {
    }

    @Test
    @DisplayName("Verificando se o método calcular retorna uma soma correta")
    public void verificarSeOMetodoCalcularRetornaSomaCorreta() {
        //Cenário
        int a = 3;
        int b = 2;
        EOperator operador = EOperator.soma;

        //Executar
        double c = operador.calcular(a, b);
        
        //Verificar
        Assertions.assertEquals(5, c);
    }

    @Test
    @DisplayName("Verificando se o método calcular retorna uma divisão correta")
    public void verificarSeOMetodoCalcularRetornaDivisaoCorreta() {
        //Cenário
        int a = 6;
        int b = 2;
        EOperator operador = EOperator.divisao;

        //Executar
        double c = operador.calcular(a, b);
        
        //Verificar
        Assertions.assertEquals(3, c);
    }


    @Test
    @DisplayName("Verificando se o método calcular retorna uma multiplicação correta")
    public void verificarSeOMetodoCalcularRetornaMultiplicacaoCorreta() {
        //Cenário
        int a = 3;
        int b = 2;
        EOperator operador = EOperator.multiplicacao;

        //Executar
        double c = operador.calcular(a, b);

        //Verificar
        Assertions.assertEquals(6, c);
    }

    @Test
    @DisplayName("Verificando se o método calcular retorna uma subtração correta")
    public void verificarSeOMetodoCalcularRetornaSubtracaoCorreta() {
        //Cenário
        int a = 10;
        int b = 5;
        EOperator operador = EOperator.subtracao;

        //Executar
        double c = operador.calcular(a, b);

        //Verificar
        Assertions.assertEquals(5, c);
    }





}
