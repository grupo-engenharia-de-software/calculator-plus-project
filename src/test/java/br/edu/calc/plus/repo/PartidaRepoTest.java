/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package br.edu.calc.plus.repo;

import br.edu.calc.plus.domain.Partida;
import br.edu.calc.plus.domain.Usuario;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/**
 *
 * @author gabri
 */

@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
@DataJpaTest

public class PartidaRepoTest {
    @Autowired
    private PartidaRepo partidas;

    Partida p1;
    Usuario u1;
    
    @BeforeEach
    public void init() {
        p1 = new Partida(1, LocalDateTime.now(), 20.0, 10);
        u1 = new Usuario(1, "joão", "joaologin", "joao@gmail.com", "123456", "Juiz de Fora", LocalDate.now());        
        p1.setUsuario(u1);
        partidas.save(p1);
    }

    @AfterEach
    public void destroy(){
    partidas.deleteAll();    
}

    @Test
    public void verificarEncontrarPorId() {
        //Cenário

        //Execução
        List<Partida>listaPartida = partidas.findByUsuarioId(1);

        //Verificação
        Assertions.assertEquals(1, listaPartida.size());

    }



    

    @Test
    public void verificarSeGetUsuarioRetornaUsuario() {
        //Cenário

        //Execução
        int IdUsuario = p1.getUsuario().getId();

        //Verificação
        Assertions.assertEquals(1, IdUsuario);
    }

    @Test
    public void verificarSeGetCidadeRetornaCidadeDoUsuarioDeUmaPartida() {
        //Cenário

        //Execução
        String cidade = p1.getUsuario().getCidade();

        //Verificação
        Assertions.assertEquals("Juiz de Fora", cidade);
    }


    
}
