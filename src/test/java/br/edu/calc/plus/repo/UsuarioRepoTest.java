/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package br.edu.calc.plus.repo;

import br.edu.calc.plus.domain.Usuario;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Optional;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/**
 *
 * @author gabri
 */
@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
@DataJpaTest


public class UsuarioRepoTest {

    @Autowired
    private UsuarioRepo usuarios;

    Usuario u1, u2;

    
    @BeforeEach
    public void init() {
        u1 = new Usuario(1, "joão", "joaologin", "joao@gmail.com", "123456", "Juiz de Fora", LocalDate.now());
        u2 = new Usuario(2, "pedro", "pedrologin", "pedro@gmail.com", "123456", "Juiz de Fora", LocalDate.of(2002, 02, 20));
        usuarios.save(u1);
        usuarios.save(u2);
    }


    @AfterEach
    public void destroy() {
        usuarios.deleteAll();
    }

    @Test
    public void verificarEncontrarNome() {
        //Cenário

        //Execução
        Optional<Usuario>listaNome = usuarios.findByNome("joão");    

        //Verificação
        Assertions.assertEquals(true, listaNome.isPresent());
    }

    @Test
    public void verificarEncontrarLogin() {
        //Cenário

        //Execução
        Optional<Usuario>listaLogin = usuarios.findByLogin("joaologin");    

        //Verificação
        Assertions.assertEquals(true, listaLogin.isPresent());
    }

    @Test
    public void verificargetSenha() {
        //Cenário

        //Execução
        String senhaUsuario = u1.getSenha();

        //Verificação
        Assertions.assertEquals("123456", senhaUsuario);

    }
    
}
