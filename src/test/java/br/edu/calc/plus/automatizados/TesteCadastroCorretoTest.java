package br.edu.calc.plus.automatizados;// Generated by Selenium IDE
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import static org.hamcrest.CoreMatchers.is;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.HashMap;
import java.util.Map;
public class TesteCadastroCorretoTest {
  private WebDriver driver;
  private Map<String, Object> vars;
  JavascriptExecutor js;
  @BeforeEach
  public void setUp() {

    System.setProperty("webdriver.chrome.driver", "chromedriver.exe");

    ChromeOptions options = new ChromeOptions();
    options.addArguments("start-maximized"); // open Browser in maximized mode
    options.addArguments("disable-infobars"); // disabling infobars
    options.addArguments("--disable-extensions"); // disabling extensions
    options.addArguments("--disable-gpu"); // applicable to windows os only
    options.addArguments("--disable-dev-shm-usage"); // overcome limited resource problems
    options.addArguments("--no-sandbox"); // Bypass OS security model
    options.addArguments("disable-dev-shm-usage");
    options.addArguments("--allowed-ips");

    driver = new ChromeDriver(options);


    js = (JavascriptExecutor) driver;
    vars = new HashMap<String, Object>();
  }
  @AfterEach
  public void tearDown() {
    driver.findElement(By.cssSelector("li:nth-child(2)")).click();
    driver.quit();
  }
  @Test
  public void testeCadastroCorreto() throws InterruptedException {
    driver.get("http://localhost:9000/");
    driver.manage().window().setSize(new Dimension(1376, 744));
    driver.findElement(By.cssSelector("li:nth-child(3) p")).click();
    driver.findElement(By.id("cpNome")).sendKeys("ademiro1");
    driver.findElement(By.id("cpLogin")).sendKeys("ademiro1");
    driver.findElement(By.id("cpSenha")).sendKeys("333333");
    driver.findElement(By.id("cpEmail")).sendKeys("ademir1o@gmail.com");
    driver.findElement(By.id("cpCidade")).sendKeys("Lisboa");
    driver.findElement(By.id("cpData")).click();
    driver.findElement(By.id("cpData")).sendKeys("01-04-2021");
    driver.findElement(By.cssSelector(".btn")).click();
    driver.findElement(By.cssSelector(".col-11")).click();
    Thread.sleep(2000);
    Assertions.assertEquals("\"cadastro criado com sucesso.\"",driver.findElement(By.cssSelector("span:nth-child(4)")).getText());
    //assertThat(driver.findElement(By.cssSelector("span:nth-child(4)")).getText(), is("\\\"cadastro criado com sucesso.\\\""));
  }
}
